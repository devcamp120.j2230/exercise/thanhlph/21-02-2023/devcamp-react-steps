import { Component } from "react";

const steps = [
    {id: 1, title: 'Hello World', content: 'Welcome to learning React!'},
    {id: 2, title: 'Installation', content: 'You can install React from npm.'},
    {id: 3, title: 'Create react app', content: 'Run create-react-app to run project.'},
    {id: 4, title: 'Run init project', content: 'Cd into project and npm start to run project.'},
  ];

  class Step extends Component{
    render() {
        return (
            <>
            <ul>
                {
                    steps.map((value, index) => {
                        return<li key={index}>Bước {value.id} {value.title}: {value.content}</li>
                    })
                }
            </ul>
            </>
        )
    }
  }

  export default Step;